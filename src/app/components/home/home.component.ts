import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../core/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  email = 'admin@theleague.com';
  password = 'Welcome1';
  error = '';

  constructor(public auth: AuthService, private router: Router) {}

  ngOnInit() {}

  emailLogin() {
    this.error = '';
    this.auth
      .emailLogin(this.email, this.password)
      .then(response => {
        this.router.navigateByUrl('/dashboard');
      })
      .catch(error => {
        this.error = 'Unsuccessful login, please try again.';
      });
  }
}
