import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../core/auth.service';

declare const $: any;
declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}
export const ROUTES: RouteInfo[] = [
  { path: '/dashboard', title: 'Dashboard', icon: 'dashboard', class: '' },
  { path: '/new-customer', title: 'New Customer', icon: 'person', class: '' },
  { path: '/new-rent', title: 'New Rent', icon: 'add', class: '' },
  {
    path: '/manage-inventory',
    title: 'Manage Inventory',
    icon: 'create_new_folder',
    class: ''
  }
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  constructor(public auth: AuthService) {}

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }
  isMobileMenu() {
    if ($(window).width() > 991) {
      return false;
    }
    return true;
  }
  logout() {
    this.auth.signOut();
  }
}
