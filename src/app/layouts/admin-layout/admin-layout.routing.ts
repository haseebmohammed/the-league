import { ManageInventoryComponent } from '../../components/manage-inventory/manage-inventory.component';
import { NewCustomerComponent } from '../../components/new-customer/new-customer.component';
import { Routes } from '@angular/router';
import { DashboardComponent } from '../../components/dashboard/dashboard.component';
import { NewRentComponent } from '../../components/new-rent/new-rent.component';

export const AdminLayoutRoutes: Routes = [
  { path: 'dashboard', component: DashboardComponent },
  { path: 'new-customer', component: NewCustomerComponent },
  { path: 'new-rent', component: NewRentComponent },
  { path: 'manage-inventory', component: ManageInventoryComponent }
];
