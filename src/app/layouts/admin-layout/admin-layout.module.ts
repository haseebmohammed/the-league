import { MaterialModule } from './../../material/material.module';
import { ManageInventoryComponent } from './../../components/manage-inventory/manage-inventory.component';
import { NewCustomerComponent } from './../../components/new-customer/new-customer.component';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { DashboardComponent } from '../../components/dashboard/dashboard.component';
import { NewRentComponent } from '../../components/new-rent/new-rent.component';
import { CoreModule } from '../../core/core.module';
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    CoreModule
  ],
  declarations: [
    DashboardComponent,
    NewCustomerComponent,
    NewRentComponent,
    ManageInventoryComponent
  ]
})
export class AdminLayoutModule {}
