import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { AuthService } from '../core/auth.service';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  constructor(private afs: AngularFirestore, private auth: AuthService) {}

  createCustomer() {}

  updateCustomer() {}

  deleteCustomer() {}

  getCustomers() {
    this.afs
      .collection('customers', ref =>
        ref.where('companyid', '==', this.auth.currentUser.companyid)
      )
      .valueChanges()
      .subscribe(response => {
        console.log(response);
      });
  }

  getCustomerById() {}
}
