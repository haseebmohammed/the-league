// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAGWedsBRELWbfAXy3k_63tXK141Cp3mvI',
    authDomain: 'the-league-ilmstudios.firebaseapp.com',
    databaseURL: 'https://the-league-ilmstudios.firebaseio.com',
    projectId: 'the-league-ilmstudios',
    storageBucket: 'the-league-ilmstudios.appspot.com',
    messagingSenderId: '593904637682'
  }
};
